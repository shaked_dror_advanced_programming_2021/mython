#ifndef INTEGER_H
#define INTEGER_H
#include "type.h"

class Integer : public Type
{
public:
	Integer(int data);


private:
	int _data;

};

#endif // INTEGER_H