#ifndef TYPE_H
#define TYPE_H


class Type
{
public:
	Type();
	~Type();
	int get_isTemp()const;
	void set_isTemp(bool isTemp);


private:
	bool _isTemp;//need to be false in the start
};

#endif //TYPE_H
