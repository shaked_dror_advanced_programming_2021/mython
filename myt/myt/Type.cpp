#include "type.h"

Type::Type():
	_isTemp(false)
{
}

Type::~Type()
{
}

int Type::get_isTemp() const
{
	return this->_isTemp;
}

void Type::set_isTemp(bool isTemp)
{
	this->_isTemp = isTemp;
}
